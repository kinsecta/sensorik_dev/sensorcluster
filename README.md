# Inhalt
1. KInsecta Sensorcluster Principle
2. Construction Manual
3. Initial Operation
4. Measurement Protocol
5. Shopping List


__________________________
# 1. KInsecta Sensorcluster

<img width="889" height="500" src="Camera/fotos_aufbau/frontispiz.png?raw=true"/> \


This is a guide to build the differnt sensors used to collect images, wing beats and weather data. 

Insects are caugth by traditional monitoring traps and approach then light. On their way out, they first pass through a camera system where, thanks to an IR motion sensor, a photo is saved. Then they fly through an optical wing beat sensor and are released. Additional sensors are used to collect weather conditions.

The setup is RaspberryPi based. All collected data can be found in the following Cloud: LINK?

In the above folders, you can find a step to step guide to build the different sensors:
* Customized Printed Circuit Board to control all the sensors
* Camera Sensor 
* Wing Beat Sensor
* Weather Sensors (WORK IN PROGRESS)

<img width="500"  src="Camera/fotos_aufbau/ShowCase.jpg"/> \

## 2. Construction Manual

The construction can be divided into 4 sub-steps before all parts including environmental sensors are assembled:
1. We recommend to start with the preparation of the KInsecta board. It operates the light barriers and supplies the Raspberry Pi with power. [(board manual and source files)](https://gitlab.com/kinsecta/sensorik_dev/sensorcluster/-/tree/master/Board)
2. Building of the camera module [(camera manual and source files)](https://gitlab.com/kinsecta/sensorik_dev/sensorcluster/-/tree/master/Camera)
3. Building of the wingbeat sensor [(wingbeat sensor manual and source files)](https://gitlab.com/kinsecta/sensorik_dev/sensorcluster/-/tree/master/Wingbeat_Hardware)
4. Set up the browser based interface Bokeh on the Raspberry Pi for [setup guide](https://gitlab.com/kinsecta/bokeh_dashboards/Multisensors_Bokeh).

## 3. Initial Operation

## 4. Measurement Protocol

following soon \
<img width="900"  src="Camera/fotos_aufbau/Measurement.png"/> \

## 5. Shopping List
Info: assembled circuit boards & light barrier components can be ordered via info@kinsecta.org. In this case, only the parts that have a * must be ordered. Also for the electronics of the Wingbeat Sensor.

### Connectors:

* [4x 4-PIN Female Connectors (JST PH4-P BU)](https://www.reichelt.de/jst-buchsengehaeuse-1x4-polig-ph-jst-ph4p-bu-p185043.html?&trstct=pos_3&nbc=1)
* [3x 4-PIN Male Connectors (JST PH4-P ST or B4B-PH-K(LF)(SN))](https://www.reichelt.de/jst-stiftleiste-gerade-1x4-polig-ph-jst-ph4p-st-p185051.html?&trstct=pos_0&nbc=1)
* [9x 2-PIN Female Connectors (JST PH2-P BU)](https://www.reichelt.de/de/en/jst-socket-housing-1x2-pin-ph-jst-ph2p-bu-p185041.html?&trstct=pos_2&nbc=1)
* [7x 2-PIN Male Connectors (JST PH2-P ST or B2B-PH-K(LF)(SN))](https://www.reichelt.de/de/en/jst-pin-header-straight-1x2-pin-ph-jst-ph2p-st-p185049.html?&trstct=pos_5&nbc=1)
* [2x 2-PIN Male Connectors (JST PH2-P ST90 (horizontal))](https://www.reichelt.de/de/en/jst-pin-header-90-1x2-pin-ph-jst-ph2p-st90-p185056.html?&trstct=pos_1&nbc=1)
* [1x 4-PIN Male Connectors (JST PH4-P ST90 (horizontal))](https://www.reichelt.de/de/en/jst-pin-header-90-1x4-pin-ph-jst-ph4p-st90-p185058.html?&trstct=pos_5&nbc=1)
* [40x JST crimp contact](https://www.reichelt.de/de/en/jst-crimp-contact-socket-ph-jst-ph-cks-p185072.html?GROUPID=8558&START=0&OFFSET=16&SID=93a193250b9636b94ba59415a84ddce6458fe20a9e0000de54cf0&LANGUAGE=EN&&r=1)
* Recommended Alternative for the connectors above: [JST CONNECTOR SET](https://www.amazon.de/CQRobot-St%C3%BCcke-JST-PH-Steckverbinder-Weiblich/dp/B0731MZCGF/ref=sr_1_5?crid=3P2XG0ZVWGZ5A&keywords=jst+ph&qid=1658840048&sprefix=%2Caps%2C52&sr=8-5)
* [1x Double Stack USB-A](https://www.digikey.de/de/products/detail/w%C3%BCrth-elektronik/61400826021/2060603) 
* [recommend a: 1x Barrel Jack 2.1X5.5MM SOLDER](https://www.digikey.de/de/products/detail/w%C3%BCrth-elektronik/694106301002/5047522)
* [alternative a: 1x Barrel Jack 2.5X5.5MM SOLDER](https://www.digikey.de/de/products/detail/w%C3%BCrth-elektronik/694108301002/5047524)
* [1x Barrel Jack Adapter](https://www.amazon.de/Stecker-Netzadapter-Konverter-Anschl%C3%BCsse-CCTV-Kamera-Anwendungen/dp/B08R7328VF/ref=sr_1_1?__mk_de_DE=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=GGVO6UEFLIJ1&keywords=barrel+jack+adapter+2%2C1+to+2%2C5&qid=1658841492&sprefix=barrel+jack+adapter+2+1+to+2+5%2Caps%2C58&sr=8-1) is only needed for Barrel Jack 2.5x5.5mm Solder and not for 2.1x5.5mm
* [recommend b: 2x Female connector strip, 3-pin, RM 2.54, straight](https://www.digikey.de/de/products/detail/adam-tech/RS1-03-G/9832055)
* [recommend b: 2x Female connector strip, 6-pin, RM 2.54, straight](https://www.digikey.de/de/products/detail/sullins-connector-solutions/PPPC061LFBN-RC/810178) 
* [alternative b: 1x Female connector strip, 1x 40-pin, RM 2.54, straight](https://www.berrybase.de/en/buchsenleiste-1x-40-polig-rm-2-54-gerade) a female header with 40 pins, which has to be cut to 2x 6 pins and 2x 3 pins as alternative
* [1x Stacking Header für Raspberry, 2x20](https://www.berrybase.de/stacking-header-f-252-r-raspberry-2x20-61-40-polig-rm-2-54?c=318)
* [3x Jumper, RM 2.54 (1 camera 2 wingbeat)](https://www.reichelt.de/de/en/jumper-black-rm-2-54-jumper-2-54-sw-p9017.html?&trstct=pos_5&nbc=1)
* [1x Pin headers, 1x2 RM 2.54](https://www.reichelt.de/de/en/pin-headers-2-54-mm-1x02-straight-mpe-087-1-002-p119879.html?&nbc=1&trstct=lsbght_sldr::9017)


### Electronic Components:

* [Raspberry Pi 4](https://www.raspberrypi.com/products/raspberry-pi-4-model-b/) *
* [2x IS471FE OPIC](https://www.mouser.de/ProductDetail/Sharp-Microelectronics/IS471FE?qs=5S%2F4hkdqNNcYUD3qW%252BMMdQ%3D%3D) (OPIC Light Barrier Transmitter)
* [2x TRU COMPONENTS 1577519 IR-Diode 940 nm 30 ° 5 mm radial](https://www.conrad.de/de/p/tru-components-1577519-ir-diode-940-nm-30-5-mm-radial-bedrahtet-1577519.html) (IR-LED)
* [1x Power supply 12V DC 3,0A with barrel jack 5,5x2,1mm](https://buyzero.de/products/meanwell-schaltnetzteil-12v-dc-3-0a-mit-hohlstecker-5-5x2-1mm) *
* [1x Logic Level Converter](https://eckstein-shop.de/SparkFunLogicLevelConverter-Bi-DirectionaShifterPegelwandlerEN)
* [2x IRF540ZPBF MOSFET](https://www.conrad.de/de/p/infineon-technologies-irf540zpbf-mosfet-1-n-kanal-92-w-to-220ab-160884.html)
* [1x BTF-LIGHTING FCOB COB Cool White 6000K Flexible High Density Light Dimmable LED Strip CRI90 + 1M528LEDs DC12V 70W Flexible FOB LED Strip](https://www.amazon.de/BTF-LIGHTING-Nat%C3%BCrliches-Verformbar-Schlafzimmer-Innendekoration/dp/B0915K69C6/ref=sr_1_4_sspa?__mk_de_DE=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=17KBMCWQ0DL3T&keywords=led+cob+strip+12v+kaltwei%C3%9F&qid=1651745056&sprefix=led+cob+strip+12v+kaltwei%C3%9F,aps,92&sr=8-4-spons&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUExRU5BTERWNlo1TzVBJmVuY3J5cHRlZElkPUEwOTA1MjU5MkdCNksxT0g0UkdWRSZlbmNyeXB0ZWRBZElkPUEwMjk3MTYwM1FHMzVEV1RUQUM4VSZ3aWRnZXROYW1lPXNwX2F0ZiZhY3Rpb249Y2xpY2tSZWRpcmVjdCZkb05vdExvZ0NsaWNrPXRydWU&th=1&language=en_GB&currency=EUR)


### Wingbeat Sensor:
* 1x AddFilter
    * [1x IC OPAMP GP 1 CIRCUIT SOT23-5](https://www.digikey.de/de/products/detail/stmicroelectronics/TS971ILT/1040417?s=N4IgTCBcDaICwE4DsBaAHARgKwAYUZQDkAREAXQF8g)
    * [1x LDO Regulator](https://www.mouser.de/ProductDetail/Nisshinbo/RP170N451D-TR-FE?qs=sGAEpiMZZMsGz1a6aV8DcPnFGm5zzqFswLyCXwndeiA%3D)
* 2x Receiver
    * [12x Sensor Photodiode 940NM 60°](https://de.farnell.com/on-semiconductor/qsb34gr/pin-fotodiode-940nm-plcc-2/dp/2825072)
* 2x Emitter
    * [2x Emitter IR 940NM 70MA SMD](https://www.digikey.de/de/products/detail/vishay-semiconductor-opto-division/VSMY12940/5325807?s=N4IgTCBcDaIG4GcC2BPAjGAnAFgAwGMAXAWgDsATEAXQF8g)
    * [1x IC OPAMP GP 1 CIRCUIT 8SOIC](https://www.digikey.de/de/products/detail/stmicroelectronics/TS971IDT/1040416?s=N4IgTCBcDaICoGUCcB2AjASQCJxAXQF8g)

* [1x DELOCK 63926 Soundkarte, extern, 7.1, USB 2.0, 24 bit / 96 kHz](https://www.reichelt.de/soundkarte-extern-7-1-usb-2-0-24-bit-96-khz-delock-63926-p287907.html)

### Optics:

* [1x 2,25" x 12", 2" Brennweite, Zylinderfresnellinse](https://www.edmundoptics.de/p/225quot-x-120quot-20quot-fl-cylinder-fresnel-lens/6961/) has to be cut in 2 equal pieces

### Sensors:

* [Raspberry Pi High Quality Camera](https://www.raspberrypi.com/products/raspberry-pi-high-quality-camera/)* 
* [16mm 10MP Telephoto Lens for Raspberry Pi HQ Camera - 10MP](https://www.adafruit.com/product/4562) * 
* [Ribbon Cable for Raspberry Pi Camera (min. 50cm)](https://www.ebay.de/itm/402651404480?chn=ps&_trkparms=ispr%3D1&amdata=enc%3A1dt6MnyhVTY6YPZvbhhWJyA45&norover=1&mkevt=1&mkrid=707-134425-41852-0&mkcid=2&itemid=402651404480&targetid=1396098981054&device=c&mktype=pla&googleloc=9061132&poi=&campaignid=14472331666&mkgroupid=125370287694&rlsatarget=pla-1396098981054&abcId=9300652&merchantid=7511942&gclid=CjwKCAiAxJSPBhAoEiwAeO_fP7I64K0W2ql5qhkid-j6wCUAmQkpWmZpumvZOCIidIjAbiu12R2ZMhoCIVcQAvD_BwE)*
* [1x Adafruit BME280](https://www.berrybase.de/adafruit-bme280-i2c-oder-spi-temperatur-feuchtigkeits.-druck-sensor)*
* [1x Adafruit PCF8523 Real Time Clock RTC](https://eckstein-shop.de/Adafruit-PCF8523-RTC)*
* [1x Adafruit BH1750](https://www.berrybase.de/sensoren-module/licht/adafruit-bh1750-licht-sensor-stemma-qt/qwiic?number=ADA4681)*
* [1x Adafruit AS7341 10-Channel Light / Color Sensor Breakout](https://eckstein-shop.de/AdafruitAS734110-ChannelLight2FColorSensorBreakout)*
* [1x Adafruit PMSA003I](https://www.berrybase.de/adafruit-pmsa003i-luftqualitaets-breakout)*

### Others:

* Heat-shrink tubing (2-to-1mm & 5-to-2.5mm & 6-to-3mm)
* [1x TRU COMPONENTS 1565203 Strand 2 x 0.14 mm² Black, Red 50 m](https://www.conrad.com/p/tru-components-1565203-strand-2-x-014-mm-black-red-50-m-1565203)
* [1x USB A to USB C cable min. 0,4 m](https://www.reichelt.de/de/en/usb-3-0cable-a-plug-auf-c-plug-blue-0-5-m-click-45123-p325678.html?&trstct=pos_7&nbc=1)*
* [Micro HDMI Cable](https://www.reichelt.de/de/en/hdmi-micro-hdmi-cable-pureinstall-series-1-50m-pure-pi1300-015-p135163.html?&trstct=pos_3&nbc=1)*
* [1x SparkFun Qwiic - cable, 500mm](https://www.berrybase.de/sparkfun-qwiic-flexibles-kabel-500mm)*
* [5x SparkFun Qwiic - cable, 50mm](https://www.berrybase.de/sensoren-module/adafruit-stemma-qt-sparkfun-qwiic/kabel/sparkfun-qwiic-flexibles-kabel-50mm)*
* [2x spring loaded pins](https://www.amazon.de/-/en/Pogopin-Diameter-Length-Spring-Pressure/dp/B07WP196KW/ref=sr_1_8?crid=6FKCYWGQERMM&keywords=Federkontaktstiften&qid=1644418449&s=instant-video&sprefix=federkontaktstiften%2Cinstant-video%2C64&sr=1-8)
* Soldering iron
* Screwdriver
* Wood Glue
* Superglue
* Black Wood Spray Paint
* Spiral Cable Wrap Tubing 16mm

### Wood:

* 6mm wooden board 
* 4mm wooden board 

### Plexiglas:

* Plexiglas 1: WH 10, light transmission = 62%, thickness = 3mm 
* Plexiglas 2: Transparent, thickness = 3mm 

### Screws, Spacers & Nuts:

* [1x threaded rod M6x1000mm](https://www.bauhaus.info/gewindestangen/profi-depot-gewindestange-vz/p/10827379?gclid=CjwKCAiAjPyfBhBMEiwAB2CCIpe8lxNwNQuoi1vcsCYLzmBpR7r3V7kyXqdx7_PdWNrB6FgE-QjSrhoCAGAQAvD_BwE&ef_id=CjwKCAiAjPyfBhBMEiwAB2CCIpe8lxNwNQuoi1vcsCYLzmBpR7r3V7kyXqdx7_PdWNrB6FgE-QjSrhoCAGAQAvD_BwE:G:s&s_kwcid=AL!5677!3!402866511592!!!g!370050940006!&cid=PSEGoo8400533753_85436250429&pla_campid=8400533753&pla_adgrid=85436250429&pla_prpaid=370050940006&pla_prid=10827379&pla_adt=pla&pla_prch=online&pla_stco=)

* screws:
    * 18x M2.5x8
    * 12x M2.5x16
    * 12x M3x16
    * 6x M4x10
    * 8x M5x18

* nuts:
    * 18x M2.5 (M2.5, M3)
    * 12x M3
    * 32x M6 

* spacer 
    * 2x M2.5x6
    * 4x M2.5x20 (Male-Female)
    * 4x M2.5x20 (Female-Female)
    * 3x M4x35
* washers (M3, M4)
    * M3
    * 3x M4
* serrated washer
    * 32x M6 
* screw sockets (M5)
    * 8x M5


(last update: 12.05.2023)
________________________



