# Wingbeat Sensor

Build your DIY wingbeat sensor and become part of the KInsecta team.

<img width="550" height="300" src="images/Wingbeat_bearbeitet_frei.png?raw=true"/>

## Materials

### 3D Print

All .stl files to be printed can be found in the folder "3D print"

* 2 x Fresnel_lensholder_v20.stl
* 1 x receiver_v20.stl
* 1 x emitter_v20.stl
* 4 x Fresnel_wedge_v20.stl


### Cut

Lasercutter: All files to be cutted can be found in the folder "Cut".

Wood thickness blue parts: **4mm**.
Wood thickness wb_case_socket: **6mm**.


<img width="550" height="300" src="images/wb_case_parts.png?raw=true"/>

### Electronics

Electronic schematics can be found in the folder "files" and the corresponding kicad-files in "electronics"

* add_filter.pdf
* add_filter.pdf
* receiver_v20.pdf
* emitter_v20.pdf

### Tools

Additional material so as an oder list für electronic components can be found in "files" too.

## How to do it
### Wiring

[How to](./files/FresnelDetektor_Zusammenbau.pdf)

The boards are wired as shown in the following figure


<img width="700" src="images/FresnelDetektor_Zusammenbau_1.png"/>
<img width="700" src="images/FresnelDetektor_Zusammenbau_2.png"/>

### Case

The sensor must be shielded from external sources of interference. Two different covers are available for this purpose, one 3D printed and the other made of wood. 

Parts for the first one are available in the **3D print** directory. They should be printed with dark filament. The 3d print for the cover takes several hours.

Otherwise, the files for the wooden cover can be found in the **Cut** directory. Cutting and assembling the wooden cover only takes a few minutes. The assembly of the parts is illustrated in the following picture.

<img width="700" src="images/wb_case_aufbau.png"/>

Next, the scocket supports must be positioned as shown in the following schematic image. Before gluing, carefully screw in the four sockets, paying attention to the correct position. The correct position of the os sockets is shown in the last pictures. The sockets must be screwed in from the back and not completely through the part, as shown in the last picture. Apply superglue to the sockets before screwing it in.

<img width="700" src="images/wb_case_aufbau2.png"/>

<img width="300" src="images/muffen1.jpg"/> <img width="300" src="images/muffen2.jpg"/> <img width="300" src="images/muffen3.jpg"/>


