# KInsecta Board

How to build your Board step by step.

<img align= 'center' width="423" height="375" src="imgs/board_main_V2.png"/>

## Materials
see [Shopping List](https://gitlab.com/kinsecta/sensorik_dev/sensorcluster/-/blob/master/README.md)


(last update: 14.03.2023)


## How to do it (a tutorial for assembling) for assembled boards the work starts at step 5
1. You can order our customized printed circuit board by using the files in the "gerber_file" folder.
We also can ordered it for you info@kinsecta.org.

<img align= 'center' width="400" src="imgs/IMG_2355Standardplatine_Hersteller_1.jpg"/>


---

2. The first thing to do is to solder the JST connectors to the board. The pictures show the alignment of the connectors (3x JST PH4 and 3x JST PH2) and the soldering points from the back side.

<img height="200" src="imgs/IMG_2357_JST Stecker Positionen 2er4er_2.jpg"/> 
<img height="200" src="imgs/IMG_2359 Lötstellen_3.jpg"/>

----

3. Turn the board around and solder Barrel Jack and Double Stack USB-A connector to the board.

<img height="200" src="imgs/IMG_2360_leere_Platine_andere_Seite_4_2.jpg"/> 
<img height="200" src="imgs/IMG_2361_barrelJack_USB Anschluss_5.jpg"/>

---

4. Place the MOSFETs and solder them on. Here, too, the polarity must be observed. The legs are shortened after soldering as shown on the back side. 

<img height="200" src="imgs/IMG_2364%20Plazierung%20der%20MOSFETs_orientierung_6.jpg"/> 
<img height="200" src="imgs/IMG_2366_festl%C3%B6ten%20und%20eink%C3%BCrzen_7.JPG"/>

---

5. Now the Logig Level Converter is placed on the board. The easiest way is to connect it to the 2 female connector strips first and then solder them. This way the alignment is correct and the connectors are not tilted. Note the labeling (e.g. LV1 to LV1,...)

<img height="200" src="imgs/IMG_2370_LevelConverter_LV1_LV1_8.jpg"/>
<img height="200" src="imgs/IMG_2373erst%20LevelConverter%20auf%20femal%20pin%20Header%20stecken%20vor%20loeten_9.JPG"/>

---

6. The KInsecta Header is almost ready. It can be plugged onto the Raspi. Use the Stacking Header for Raspberry Pi 2x20 pins and the spacer sleeves and solder.

<img height="200" src="imgs/IMG_2376_Distanzst%C3%BCcke%20auf%20Raspi%20und%20Pinheader%202x20%20aufstecken_10.jpg"/>
<img height="200" src="imgs/IMG_2377%20darauf%20kann%20die%20Platine_11.jpg"/>
<img height="200" src="imgs/IMG_2379%20fertig%20verloetet_12.jpg"/>

---

7. The 12V switch1 can be used to switch the light on and off in the arena. In the default use case we omit this function and so it is important to short-circuit these pins with a jumper.

<img height="200" src="imgs/IMG_2380%20Schalter%20f%C3%BCr%20die%20Beleuchtung%20an%20aus%20optional%20-%20hier%20jumper%20notwendig_13.jpg"/>

---

8. The next step is very important! The Raspberry Pi can be damaged and **must not** be connected until the correct voltage of 5.1V is provided. An adjustable step down module is implemented. Via a potentiometer screw the voltage must be adjusted from approx. 12V to 5.1V. Now the voltage of 5.1V is provided by a USB socket of the KInsecta board and can be connected to the USB-C power socket of the Raspberry Pi (right image). You have to screw for some time before anything happens. If the screw 'clicks' when you turn it, you are at the stop and turn the other direction. 

<img height="200" src="imgs/IMG_2381%20an%20Netzteil%20anschlie%C3%9Fen%20und%20Spannung%20an%20USB%20stecker%20messen_14.jpg"/>
<img height="200" src="imgs/IMG_2383_Spannungsregler_Poti_15.jpg"/>
<img height="200" src="imgs/IMG_2384%20auf%205.1V%20regeln%20WICHTIG_15.jpg"/>
<img height="200" src="imgs/IMG_9638_bearbeitet.jpg">

----

9. The LED strips must be fixed to the side boards. They have adhesive strips on their back. Here, too, the polarity must be considered (2nd picture from the left). `+` must face upward at one stripe and down at the next. They should be placed as close as possible, hence overlapping. **Tip**: start with the outer 2 stribes and fix the third one afterwards. So the electrodes have to be shortened a bit (about 1mm see picture 3) so that you can solder both contacts. Use enough of solder. The contact areas are large all 3 contact points have to be connected. 

<img height="200" src="imgs/IMG_2389_Seitenplatinen_mit_LED_Streifen_16.jpg"/>
<img height="200" src="imgs/IMG_2390_Polung_beachten_17.jpg"/>
<img height="200" src="imgs/IMG_2391_einkuerzen_wegen_ueberlappung_etwa_1mm_18a.jpg"/>
<img height="200" src="imgs/IMG_2398_reichlich_Lot_20a.jpg"/>

---

10. 4 JST connectors 2pins are mounted on the rear side of the side modules and 2 on angle JST connectors (horizontal) for power connection on the front side. Another on angle JST 4pin connector (horizontal) for the light barriers is mounted on the front.

<img height="200" src="imgs/IMG_2406_Stecker%20kommen%20auf%20die%20R%C3%BCckseite_Polung%20beachten_23.jpg"/>
<img height="200" src="imgs/IMG_2407_gewinkelte%20JST%20Stecker%202x2%20Power1x4%20Lichtschranke_24.jpg"/>

---

11. Prepare all wires (12 x 70mmm single wires) and heat shrink tubing (12 x 15mm). Strip the wires and twist them a little.

<img height="200" src="imgs/IMG_2410_7cm%20Kabelmass_26.jpg"/>
<img height="200" src="imgs/IMG_2412%2015mm%20lange%20Schrumpfschlauche%202mm%20auf%201mm_27.jpg"/>
<img height="200" src="imgs/IMG_2413_6Stueck%20doppeladrig%20auseinandergezogen_28.jpg"/>
<img height="200" src="imgs/IMG_2416%20enden%20abisolieren%20ca%205mm%20und%20vertwisten_30.jpg"/>

Furthermore you need 8 x 60 cm wires and 4 x 30 cm wires. The endings are crimped.
Fix crimp contacts to the wires and connect to female connectors. ATTENTION: polarity is very important - follow the image series for the orientation:
4 long wires (60cm) are connected from left to right "+ - + -" (red-black-red-black) to the female connectors (closed side up)

<img height="200" src="imgs/IMG_2431_Stecker4er_6.jpg"/>
<img height="200" src="imgs/IMG_2434_Kabel_Stecker_Verbindung_4er_5.jpg"/>

2 x 2 long wires (60cm) are connected "- +" (black-red) to the female connectors (closed side up)

<img height="200" src="imgs/IMG_2434_Kabel_Stecker_Verbindung_lang_4.jpg"/>
<img height="200" src="imgs/IMG_2431_Stecker2er_1.jpg"/>

2 x 2 short wires (30cm) are connected "- +" (black-red) on one side and twisted on the other side (ATTENTION) in the corresponding connector the polarity is "+ -" (red-black) and allways refered to female connectors with closed side up.

<img height="200" src="imgs/IMG_2434_Kabel_Stecker_Verbindung_3.jpg"/>
<img height="200" src="imgs/IMG_2431_Stecker2er_2.jpg"/>

---

12. the next step is the assembly of the IR-LEDs and the light barriers IS471. First shorten the electrodes, connect wires and use shrinking tubs. This procedure has to be repeated for 2 LEDs and 2 Light barriers.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <img height="200" src="imgs/IMG_2414%20kuerzen%20der%20Pins%20der%20IR%20LED%20auf%205mm%20ACHTUNG%20Polung%20merken_29.jpg"/>
<img height="200" src="imgs/IMG_2419_start_soldering.JPG"/>
<img height="200" src="imgs/IMG_2422_soldered_LED.JPG"/>

... for the light barriers ...
<img height="200" src="imgs/IMG_2408_Lichtschranken_IS471_25_a.jpg"/>
<img height="200" src="imgs/IMG_2423.JPG"/>
<img height="200" src="imgs/IMG_2424_soldered_light_barrier.JPG"/>
<img height="200" src="imgs/IMG_2425_shrinkingtubes.jpg"/>


connect side parts to IR-LEDs and light barriers respectively <br>
The upper light barrier consists of IR-LED (D2) and Transmitter (U2) on the electronic circuit boards and the bottom one of IR-LED (D1) and Transmitter (U1). So the correct wiring is crossover - connect D2 to J10 and D1 to J9 (obvious see contacts on the circuit boards)
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  <img height="200" src="imgs/IMG_2428_LED_mount.jpg"/>
<img height="200" src="imgs/IMG_2429_connected_light_barriers.jpg"/>
<br>
Finish by connecting the two sides
<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <img height="200" src="imgs/IMG_2436_complete.jpg"/>
