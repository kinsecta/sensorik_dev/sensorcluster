# Content

1. [Our recommendations](# Our recommendations)<br>
1.1 [Variation 1](## Variation 1: Simple solution - 12V Battery)<br>
1.2 [Variation 2](## Variation 2: Solar Powered)<br>
2. [In detail](# In detail)<br>
2.1 [Battery](## Battery)<br>
2.2 [Solar Power](## Solar Power)<br>
2.3 [Charge Controller](## Charge Controller)<br>
3. [Saving energy](# Saving energy)<br>

<img width="800" height="400" src="Images/Schema_RasPi.png?raw=true"/>

# Our recommendations

To turn the KInsecta system into a truly self-sufficient the Energy Management is crucial. Following are two ways to solve this step. Before buying new materials and components, the needs, the time and know-how available should be determined.

Please view also the PowerPoint presentation from the 24. of October.   

## Variation 1: Simple solution - 12V Battery

The cheapest and fastest method is to run the Raspberry Pi using a battery. Online batteries are offered in the format of power banks. <ins> The Battery needs to be recharged regularly! </ins>

<ins>ATTENTION!</ins> Since the system is currently powered by a 12V power adapter, this battery has to have the same Voltage.

In the following is a table with the battery capacity in relations with estimated time (<ins>theoretical</ins>) the KInsecta System could run:

| Battery Capacity  | Voltage (stays the same) | estimated running time  |
| :----------------: | :------: | :----: |
| 1.7 Ah |   12 V   | 4 hours |
| 2.6 Ah |   12 V   | 6 hours |
| 3.5 Ah |   12 V   | 8 hours |
| 4.3 Ah |   12 V   | 10 hours |
| 5.1 Ah |   12 V   | 12 hours |
| 10.25 Ah |   12 V   | 24 hours |
| 20.5 Ah |   12 V   | 48 hours |


## Variation 2: Solar Powered

The needed power can be produced with a battery, solar panel and charge controller. Here is a example for the specifications of the components:

<img width="800" height="400" src="Images/EnergyManagement.png?raw=true"/>

* **Battery** <br>
_20 Ah_ lithium battery (can be fully decharged) <br>
AGM batteries can be used but should not be decharged lower than 50% to prevent irreversible damages!

* **Solar panel** <br>
_50 Wp_ directed south with a _40° angle_

* **Charge Controller** <br>
MPPT charge controller for optimal efficiency

# In detail

## Battery

### Power Consumption of the system

Regular consumption of the Raspberry Pi 4 equals to around 3 W. <br>
Embedded in the KInsecta system with the bokeh running the consumption raises up to around _5.1 V_ with _0.8 A_ resulting in **4.1 W** usage adding up to **98.1 Wh per 24 hours**. <br>
An insect in the arena triggers the foto mechanism and the LED strips with a usage of _14 W per Meter_. With _6 * 7 cm_ LED strips over _10 Frames_ the _Power Consumption of the LED can be ignored_.

### Needed Battery Size

The needed capacity of the battery can be calculated with the following formula:

$$ Capacity  = \frac{Time * PowerConsumption}{BatteryCurrent * Efficiency} $$

* Time: running time of the system
* Power Consumption: Power used by the system (4.1 W)
* Battery current: current of the given battery (usual: 12 V or  3.7 V)
* Efficiency: Efficiency of the battery (usual: 0.8)

To have the system run for _48 hours_ and a _12 Volt_ battery a capacity of **20 Ah** is needed.


## Solar Power

### Power harvest for 50 Watt solar panel

Calculation online show a power harvest of around _6 kWh per month_ from _April to August_ (see https://www.solarserver.de/pv-anlage-online-berechnen/ ). This includes the harvest on cloudy days.

Harvest of a **50 W* module (directed south, 40° angle, in Berlin): **200 Wh per 24 h** <br>
Therefore a _12 V_ battery with a capacity of **20 Ah** is charged throughout the day.

### Type of Modul

* Mono- or Polycrystalline <br>
Generally monocrystalline Modules have a higher efficiency than polycrystalline. The monocrystalline Modules are still preferred even with a higher temperature coefficient.

* Bypass Diode <br>
Bypass diodes should be build into the solar modules to prevent damaging shaded cells.



<!---
  https://www.solaranlagen-portal.com/photovoltaik/elektroheizung/ertrag-im-winter#:~:text=Ist%20es%20im%20Sommer%20bewölkt,bis%20150%20Watt%20pro%20m2.

  https://opendata.dwd.de/climate_environment/CDC/grids_germany/monthly/radiation_diffuse/

  https://desktop.arcgis.com/de/arcmap/10.3/manage-data/raster-and-images/esri-ascii-raster-format.htm

  https://www.dwd.de/DE/leistungen/solarenergie/download_dekadenbericht.pdf?__blob=publicationFile&v=6

-->


## Charge Controller



A charge controller is necessary for connecting the solar panel, battery and the power consumer. There are mainly two controller to choose from.

* PWM Controller:
The Pulse-Width-Modulation controller lowers the voltage that comes in from the solar panel to the voltage needed by the system resulting in the energy loss.

* MPPT Controller:
The Maximum-Power-Point-Tracking controller scans for the maximum performance of the input by the solar power and adapts the current.

### Comparison

Generally the MPPT charge controller is a better choice, since it is more efficient variations of the input power from the solar panel which is depending on the the operation temperature and overall weather. With higher costs and a rather smaller solar power plant the difference in efficiency can be viewed as a nice to have (see https://www.victronenergy.com/upload/documents/Technical-Information-Which-solar-charge-controller-PWM-or-MPPT.pdf ).


# Saving energy

The extra costs for a bigger battery or a MPPT controller could be invested into a power management system. Various options are being researched.

If you have experiences with the Witty Pi or have other ideas on implementing a Wake up/Sleep cycle, please get in touch with us!

## Witty Pi 4

Current research: Implementation of the WittyPi. Any tips or notes of experiences are appreciated.

<img width="200" height="200" src="Images/WittyPi.jpg?raw=true"/>

The Witty Pi 4 is a Raspberry Pi Hat with a Real-Time-Clock and a Power Management. The on-board DC to DC converter accepts up to 30 Volts and converts the Input (P2) to the 5 Volts required by the Raspberry Pi. The Real-Time-Clock enables the Witty Pi to make the Raspberry Pi to 'fall asleep'. The system can be shut down during periods in which the system does not measure - for example over night when insect activities are low.

https://www.uugear.com/product/witty-pi-4/

https://www.uugear.com/doc/WittyPi4_UserManual.pdf
