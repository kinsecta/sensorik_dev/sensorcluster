# Camera Sensor

<img width="600"  src="Camera/fotos_aufbau/Prototyp3D_freigestellt.png"/>

<img width="700"  src="Camera/fotos_aufbau/dim_for_camera02.png"/>

Useful dimensions for developing a new Camera Box are shown in the figure above.

How to build your Camera Sensor step by step. (work in progress, last edit:16.03.2023).

 

## Materials

### 3D Print

All .stl files to be printed can be found in the folder "3D-Print".

* 1x chip_adapter
* 2x arena_inner
* 2x arena_outer
* 2x fixer
* 2x washer 

#### Camera Box
Lasercutter: All files to be cutted can be found in the folder "Cut".

Wood thickness: **6mm**.

**Note**: the parts only fit together with the specified thickness of 6mm!

* box_side_cable
* box_side
* box_in
* box_out
* box_top
* box_bottom
* bottom_support_in (a + b)
* bottom_support_out (a + b)
* bottom_support_top
* top_support_out (a + b)
* top_support_top

Wood thickness: **4mm**.
* chip_support

#### Electronics Box 
Lasercutter: All files to be cutted can be found in the folder "Cut".

Wood thickness: **4mm**.

**Note**: the parts only fit together with the specified thickness of 4mm!

* picase_side_cables
* picase_side
* picase_top
* picase_bottom
* picase_front
* picase_back

**Note2**: A more sophisticated 3D box was developed by Johannes Meyer. The 3D files are in the 3D print folder under CS_Board_Case. The building instructions can be found in the PDF file "Anleitung_KInsecta_Pi-Gehäuse.pdf".

### Plexiglas Walls
Lasercutter: All files to be cutted can be found in the folder "Cut".

Plexiglas thickness: **3mm**.

* 2x plexiglas_side - plexiglas 1
* 1x plexiglas_top_bottom - plexiglas 1
* 1x plexiglas_top_bottom - plexiglas 2


### Needed Tools

* Soldering iron
* Screwdriver
* Wood Glue
* Superglue
* Black Wood Spray Paint
* 4 M2.5x16 Screws
* 4 M2.5 Nuts
* 2 M2.5x6 Spacers
* 10-12 M3x16 Screws
* 12 M3 Nuts
* 6 M4x10 Screws
* 3 M4x35 Spacers
* 4 M5x18 Screws
* 4 M5 Screw Sockets

## Needed cables

|        | <img width="120" height="140" src="fotos_aufbau/12v_kabel.png?raw=true"/> | <img width="120" height="140" src="fotos_aufbau/kurze_kabeln.png?raw=true"/> |<img width="120" height="140" src="fotos_aufbau/pogo_pins_kabel.png?raw=true"/>|<img width="60" height="140" src="fotos_aufbau/4_kabel.png?raw=true"/>|
| ------ | ------ | ------ | ------ |------ |
| type   |12V cable|short cable|pogo pin cable|IR cable|
| amount |    2   |    2   |    1   |    1   |
| length |   50cm |   30cm |   60cm |   50cm |


## How to do it

1. Begin by printing and cutting all components.
The wood thickness for the Camera Box is 6 mm. The thickness of CHIP_SUPPORT and of the Electronics Box (in blue in the figure) is 4 mm. If it is not possible to cut with a laser, the dimensions of all parts are given in the file camera_box_to_cut_final_dimension.pdf and plexiglas_to_cut_final_dimension.pdf. Paint all parts black.

<img width="650" height="406" src="fotos_aufbau/img03.png?raw=true"/><img width="350" height="250" src="fotos_aufbau/img01.png?raw=true"/><img width="480" height="270" src="fotos_aufbau/img05.png?raw=true"/><img width="500" height="300" src="fotos_aufbau/img07_2.png?raw=true"/>

----

2. Cut the LED stripes in 3 parts each containing 3 units. Prepare the LIGHT-IR BOARDS as shown in the left image. For more details check in the Board folder. Insert PLEXIGLAS_SIDE as shown in the right image. Glue the parts if they do not stay in place. Connect the two boards at the rear by joining pins J9-D1 and pin J10-D2. Prepare one cable for the light barrier and two cables for the flash lights, respecting polarity (see [Board](https://gitlab.com/kinsecta/sensorik_dev/sensorcluster/-/tree/master/Board) point 11-12). 

	<img width="400" height="175" src="fotos_aufbau/IMG_20230210_125420_1.jpg?raw=true"/> <img width="400" height="175" src="fotos_aufbau/IMG_20230210_130144.jpg?raw=true"/>

	

----
3.	The next step is to prepare the camera chip. Solder two spring-loaded pins with two 50cm cables. Black cable in this picture is ground. Cover the welding using heatshrink. Insert the cables into the CHIP_ADAPTER, making sure that the ground cable is in the left-hand opening, as shown in the fourth image. Plug the cables into the connector as shown in the last image, taking care not to confuse the cables.

	<img width="800" height="200" src="fotos_aufbau/img09-2.png?raw=true"/>


	Screw (with M2.5 screws and bolts) the resulting part with the Raspberry HQ chip and the CHIP_HOLDER, making sure that the two spring pins are in contact with the GND pin and the FSTROBE pin respectively. Use two M2.5x6 mm spacers on the other side. Place the chip and the CHIP_HOLDER with the ribbon cable on the flat side of the CHIP_HOLDER as shown in figure.
	
	<img width="300" height="400" src="fotos_aufbau/chip_edited.png"/>
	<img width="300" height="400" src="fotos_aufbau/IMG_20230220_155702.jpg"/>  

	Screw the resulting part using three M4x35mm spacers and three M4 screws with BOX_TOP. Fix the lower screws, the upper ones will be used to set the field of view. Screw the Telefoto Lens and the chip togheter placing both WASHER_CAMERA in between. Set the lens aperture at 8 and fix the corresponding screw.

	<img width="300" height="400" src="fotos_aufbau/IMG_20230316_171432_1.jpg"/>


----

4. Before proceeding with the housing, check that the camera chip, the light barrier and the flash lights are working:

	1. Insert the micro SD card into the RaspberryPi.

	2. Connect the RaspberryPi with the custom board using 4 spacers M2.5x20 and 4 M2.5x8 screws as shown in picture.

		<img width="350" height="270" src="fotos_aufbau/IMG_20230316_172638.jpg?raw=true"/> <img width="350" height="270" src="fotos_aufbau/IMG_20230316_172651_1.jpg?raw=true"/>

	3. Insert the camera cable and connect the RaspberryPi's usb c and the custum board's usb.

		<img width="350" height="350" src="fotos_aufbau/IMG_20230316_173124.jpg?raw=true"/>

	4. Connect the FSTROBE cable, flash ligh cables and light barrier cables.

		<img width="350" height="270" src="fotos_aufbau/IMG_20230316_173239.jpg?raw=true"/> <img width="350" height="270" src="fotos_aufbau/IMG_20230316_173330.jpg?raw=true"/>

	5. Connect the RaspberryPi to a screen using a micro HDMI cable; connect a keyboard and mouse.

		<img width="350" height="270" src="fotos_aufbau/IMG_20230316_174017.jpg?raw=true"/>

	6. Connect the 5V power supply. 

		 <img width="350" height="270" src="fotos_aufbau/IMG_20230316_174017_power.jpg?raw=true"/>

		The system should now start up.
		
		  <img width="400" height="235" src="fotos_aufbau/2023-03-10-164849_1920x1080_scrot.png"/>

	7. **Test 1:** open the comand prompt and type

		`raspistill -t 4000`

		If the camera is well connected, a window with the camera recording should appear. Close the window.

		<img width="400" height="235" src="fotos_aufbau/2023-03-10-165247_1920x1080_scrot.png"/>

	8. **Test 2:** Start the BOKEH App by double-clicking on the desktop icon.

		<img width="400" height="235" src="fotos_aufbau/2023-03-10-164812_1920x1080_scrot.png"/>  <img width="400" height="235" src="fotos_aufbau/2023-03-10-165654_1920x1080_scrot.png"/>

		Go to the PiCamera section and click the "Start Stream" button. Next, click the "Activate Flash" button. The lights should 	start flashing if they are well connected. Close Bokeh.

		<img width="400" height="234" src="fotos_aufbau/2023-03-10-165901_1920x1080_scrot.png"/> <img width="400" height="235" src="fotos_aufbau/2023-03-10-165914_1920x1080_scrot.png"/> <img width="400" height="235" src="fotos_aufbau/2023-03-10-170003_1920x1080_scrot_0.png"/> 

	9. **Test 3:** Hold one another the light barriers. Two lights on the custom board should blink.

		Holding the light barriers, start Bokeh and maintain contact until Bokeh is ready. Start the stream and click on "Activate Light Barrier". Test the function of the light barriers by breaking contact. After breaking contact, three pictures should be displayed on the right side. Shout down Bokeh. Disconnect the RaspberryPi and unplug all the cables. 
		
		<img width="400" height="235" src="fotos_aufbau/2023-03-10-165914_1920x1080_scrot.png"/> <img width="400" height="235" src="fotos_aufbau/2023-03-10-170003_1920x1080_scrot.png"/> <img width="400" height="235" src="fotos_aufbau/2023-03-10-170213_1920x1080_scrot.png"/>
	
		How the resulting bash activity should look like:
	
	 	<img width="400" height="235" src="fotos_aufbau/2023-03-10-170306_1920x1080_scrot.png"/>

---

5. Now we can go on with the housing.

	There are six hexagonal holes in each ARENA_INNER part. Insert M3 nuts into the holes (apply glue in the holes for extra stability).     
	**Hint:** heat the bolt with the soldering iron before inserting it into the 3D part. The part will soften and the bolt will fit more easily.

	<img width="400" height="300" src="fotos_aufbau/IMG_20230119_142523.jpg?raw=true"/>

---

6. Screw the two ARENA_INNER parts togheter with BOTTOM_SUPPORT_TOP using M3x16 screws. Insert PLEXIGLAS_TOP_BOTTOM as shown in the right image. Depending on the quality of the 3D print, inserting the Plexiglas may be difficult. Inserting it first from the opposite side and then screwing the resulting part with BOTTOM_SUPPORT_TOP might be easier. Finally, insert ARENA_OUTER on both sides and find the correct position for PLEXIGLAS_TOP_BOTTOM.

	<img width="350" height="270" src="fotos_aufbau/IMG_20230119_145430_1.jpg?raw=true"/> <img width="350" height="270" src="fotos_aufbau/IMG_20230210_123944.jpg?raw=true"/>

	Apply glue in the holes of BOTTOM_SUPPORT_OUT_B and carefully screw in two sockets. Repeat for TOP_SUPPORT_OUT_B.

	<img width="340" height="100" src="fotos_aufbau/IMG_20230119_170053.jpg?raw=true"/> <img width="340" height="100" src="fotos_aufbau/IMG_20230119_170101_1.jpg?raw=true"/>
	
	Glue BOTTOM_SUPPORT_IN/OUT_A/B and BOTTOM togheter as shown. Glue TOP_SUPPORT_OUT (A + B) and TOP in the same way. Glue TOP_SUPPORT_TOP on top of the resulting part.

	<img width="450" height="250" src="fotos_aufbau/img04_2.png?raw=true"/> <img width="400" height="300" src="fotos_aufbau/IMG_20230119_170723_1.jpg?raw=true"/>

	<img width="450" height="250" src="fotos_aufbau/img19.png?raw=true"/> <img width="400" height="300" src="fotos_aufbau/IMG_20230220_155153.jpg?raw=true"/>
----



7. Insert BOTTOM_SUPPORT_TOP (do not glue) and LIGHT-IR BOARDS into the resulting part by running the cables underneath BOTTOM_SUPPORT_TOP as shown in the picture on the left. Insert the two 12 V cables and secure all components by screwing in the two FIXERS as shown in the picture on the right. Insert the IR cable.

	<img width="400" height="400" src="fotos_aufbau/IMG_20230210_132310_1.jpg?raw=true"/> <img width="400" height="400" src="fotos_aufbau/IMG_20230210_133454_1.jpg?raw=true"/>



---

8.  All cables must be on the same side of the arena and BOX_SIDE_CABLE. Make sure that the opening of BOX_SIDE_CABLE is on the same side as the flat side of CHIP_SUPPORT, as shown in the picture. Feed all cables through the opening and glue BOX_SIDE_CABLE, BOX_IN, BOX_SIDE and BOX_TOP. Screw BOX_IN with ARENA_OUTER using M3 screws for greater stability.

	<img width="300" height="480" src="fotos_aufbau/img20.png?raw=true"/>


----
9. Now bouild the Electronics Box as shown in image.

10. Now we need to set the field of view. Insert the RaspberryPi in the Electronics Box adding 4 M2.5x20 spacers.
 **FOTO**

 Re-connect all cables (passing them througt the **TOPBOX**) and start BOKEH. 
 **FOTO**
 Start the stream. The floor of the arena is now displayed. To find the correct FoV, use the screws on the TOP. All four corners of the arena should be visible.

<img align="center" width="850" height="500" src="fotos_aufbau/img15.png?raw=true"/>


Once the FoV is set, find the correct focus placing a small item in the arena.




----
