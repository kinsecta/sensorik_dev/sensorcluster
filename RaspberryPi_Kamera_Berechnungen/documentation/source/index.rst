.. RaspberryPi Kamera Berechnungen documentation master file, created by
   sphinx-quickstart on Fri May 14 19:58:43 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to RaspberryPi Kamera Berechnungen's documentation!
===========================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   Raspi_Kamera_V2p1



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
